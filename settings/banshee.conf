#------------------------------------------------------------------------------
#
#  Banshee's general website settings
#
#------------------------------------------------------------------------------

# Database settings
#
DB_HOSTNAME = localhost
DB_DATABASE = banshee
DB_USERNAME = banshee
DB_PASSWORD = banshee

# Specify what kind of files can be uploaded via the File Administration page.
#
ALLOWED_UPLOADS = jpg|jpeg|gif|png|ico|pdf|doc|docx|xls|xlsx|txt|rtf|odt|ods|odp

# The format of the API communication. Use 'xml' or 'json'.
#
API_OUTPUT_TYPE = xml

# Default lifetime of the data in the internal cache.
#
CACHE_TIMEOUT = 86400

# Names of the days and months to be used in date_string().
#
DAYS_OF_WEEK = monday|tuesday|wednesday|thursday|friday|saturday|sunday
MONTHS_OF_YEAR = january|february|march|april|may|june| \
                 july|august|september|october|november|december

# Print error message on the screen or send them via e-mail to the administrator.
#
DEBUG_MODE = yes

# Redirect HTTP requests to HTTPS.
#
ENFORCE_HTTPS = no

# Security Headers
#
HEADER_CSP = default-src 'self' 'unsafe-inline' 'unsafe-eval'; img-src * data:
HEADER_FP = camera 'none'; geolocation 'none'; microphone 'none'

# Hide menu for unauthorized users.
#
HIDE_MENU_FOR_VISITORS = yes

# The layout to be used for the website and the CMS. Layouts can be found in
# view/banshee/layout_*.xslt. The corresponding stylesheets are located in
# public/css/banshee/layout_*.css.
#
LAYOUT_CMS = cms
LAYOUT_SITE = site

# Log all database queries to logfiles/database.log.
#
LOG_DB_QUERIES = no

# Only show menu items the current user can access.
#
MENU_PERSONALIZED = no

# Enable multilingual capabilities.
#
MULTILINGUAL = no

# The languages you can chose from in the Page Administration page.
#
SUPPORTED_LANGUAGES = en:English|nl:Nederlands

# This setting enables two-factor authentication. This requires the usage
# of a mobile app that implements RFC 6238. Examples of such app are:
#
# iOS:     https://itunes.apple.com/us/app/freeotp-authenticator/id872559395
# Android: https://play.google.com/store/apps/details?id=org.fedorahosted.freeotp
#
USE_AUTHENTICATOR = no

# Off line mode disables all modules and features of this website and shows a message.
# When set to an IP-address, only computers behind that IP-address will have normal access.
#
WEBSITE_ONLINE = 127.0.0.1
