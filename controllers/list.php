<?php
	class list_controller extends Banshee\controller {
		private function show_overview() {
			if (($lists = $this->model->get_lists()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview");

			$this->view->open_tag("lists");
			foreach ($lists as $list) {
				$this->view->record($list, "list");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_list_form($list) {
			$this->view->open_tag("edit");

			$this->view->open_tag("types");
			$this->view->add_tag("type", "Delete", array("value" => LIST_TYPE_DELETE));
			$this->view->add_tag("type", "Strike", array("value" => LIST_TYPE_STRIKE));
			$this->view->close_tag();

			$this->view->record($list, "list");

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save list") {
					/* Save list
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_list_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create list
						 */
						if ($this->model->create_list($_POST) === false) {
							$this->view->add_message("Error creating list.");
							$this->show_list_form($_POST);
						} else {
							$this->user->log_action("list %d created", $this->db->last_insert_id);
							$this->show_overview();
						}
					} else {
						/* Update list
						 */
						if ($this->model->update_list($_POST) === false) {
							$this->view->add_message("Error updating list.");
							$this->show_list_form($_POST);
						} else {
							if (is_true($_POST["unstrike"])) {
								$this->model->unstrike_list($_POST["id"]);
							}

							$this->user->log_action("list %d updated", $_POST["id"]);
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete list") {
					/* Delete list
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_list_form($_POST);
					} else if ($this->model->delete_list($_POST["id"]) === false) {
						$this->view->add_message("Error deleting list.");
						$this->show_list_form($_POST);
					} else {
						$this->user->log_action("list %d deleted", $_POST["id"]);
						$this->show_overview();
					}
				} else if ($_POST["submit_button"] == "Leave list") {
					/* Leave list
					 */
					$this->model->leave_list($_POST["id"]);
					$this->show_overview();
				} else {
					$this->show_overview();
				}
			} else if (($this->page->parameters[0] ?? null) === "new") {
				/* New list
				 */
				$list = array();
				$this->show_list_form($list);
			} else if (valid_input($this->page->parameters[0] ?? null, VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit list
				 */
				if (($list = $this->model->get_list($this->page->parameters[0])) == false) {
					$this->view->add_tag("result", "Invalid list id.");
				} else {
					$this->show_list_form($list);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
