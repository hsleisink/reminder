<?php
	class view_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function show_list($list_id) {
			$url = array("url" => "list");

			if (($list = $this->model->get_list($list_id)) == null) {
				$this->view->add_tag("result", "Invalid list id.", $url);
			} else {
				$this->view->add_javascript("jquery/jquery-ui.js");
				$this->view->add_javascript("view.js");

				$this->view->run_javascript("load_list(".$list_id.")");

				$this->view->title = $list["name"];

				$this->view->add_tag("list", null, array(
					"id"        => $list_id,
					"type"      => $list["type"],
					"maxlength" => $this->settings->list_item_max_length));
			}
		}

		private function show_list_xml($list_id) {
			if (($list = $this->model->valid_list_id($list_id)) == false) {
				$this->view->add_tag("result", "Invalid list id.", array("code" => 404));
			} else if (($items = $this->model->get_list_items($list_id)) === false) {
				$this->view->add_tag("result", "Error fetching list items.", array("code" => 500));
			} else {
				$this->view->add_javascript("jquery/jquery-ui.js");
				$this->view->add_javascript("view.js");

				$this->view->open_tag("result", array("code" => 200));
				foreach ($items as $item) {
					$item["alarm"] = show_boolean($item["alarm"] != null);
					$item["marked"] = show_boolean($item["marked"]);
					$this->view->record($item, "item");
				}
				$this->view->close_tag();
			}
		}

		private function add_item($item) {
			if ($this->model->valid_list_id($item["list_id"]) == false) {
				$this->view->add_tag("result", "Unknown list", array("code" => 404));
			} else if (trim($item["name"]) == "") {
				$this->view->add_tag("result", "Invalid item name.", array("code" => 403));
			} else if (($item_id = $this->model->add_item($item["name"], $item["list_id"])) == false) {
				$this->view->add_tag("result", "Database error.", array("code" => 500));
			} else {
				$this->view->add_tag("result", $item_id, array("code" => 200));
			}
		}

		private function strike_item($item) {
			if (($list_id = $this->model->get_list_for_item($item["id"])) == false) {
				$this->view->add_tag("result", "Unknown item.", array("code" => 404));
			} else if ($this->model->valid_list_id($list_id) == false) {
				$this->view->add_tag("result", "Invalid list id.", array("code" => 403));
			} else if ($this->model->mark_item($item["id"], $item["mark"]) == false) {
				$this->view->add_tag("result", "Database error.", array("code" => 500));
			} else {
				$this->view->add_tag("result", "ok", array("code" => 200));
			}
		}

		private function delete_item($item) {
			if (($list_id = $this->model->get_list_for_item($item["id"])) == false) {
				$this->view->add_tag("result", "Unknown item.", array("code" => 404));
			} else if ($this->model->valid_list_id($list_id) == false) {
				$this->view->add_tag("result", "Invalid list id.", array("code" => 403));
			} else if ($this->model->delete_item($item["id"]) == false) {
				$this->view->add_tag("result", "Database error.", array("code" => 500));
			} else {
				$this->view->add_tag("result", "ok", array("code" => 200));
			}
		}

		private function show_alarm_form($item) {
			$this->view->title = "Edit item";

			$this->view->add_javascript("jquery/jquery-ui.js");
			$this->view->add_javascript("banshee/datepicker.js");

			$this->view->add_css("jquery/jquery-ui.css");

			$this->view->open_tag("edit");

			$this->view->open_tag("alarm_repeat");
			$this->view->add_tag("option", "None", array("value" => ALARM_REPEAT_NONE));
			$this->view->add_tag("option", "Daily", array("value" => ALARM_REPEAT_DAILY));
			$this->view->add_tag("option", "Weekly", array("value" => ALARM_REPEAT_WEEKLY));
			$this->view->add_tag("option", "Monthly", array("value" => ALARM_REPEAT_MONTHLY));
			$this->view->add_tag("option", "Yearly", array("value" => ALARM_REPEAT_YEARLY));
			$this->view->close_tag();

			if ($item["alarm"] != null) {
				$item["alarm"] = date("l, d F Y", strtotime($item["alarm"]));
			}
			$item["marked"] = show_boolean($item["marked"]);
			$this->view->record($item, "item");

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = "List";

			if ($this->page->ajax_request) {
				/* AJAX calls
				 */
				if (($_POST["action"] ?? null) == "add") {
					$this->add_item($_POST);
				} else if (($_POST["action"] ?? null) == "strike") {
					$this->strike_item($_POST);
				} else if (($_POST["action"] ?? null) == "delete") {
					$this->delete_item($_POST);
				} else {
					$this->show_list_xml($this->page->parameters[0]);
				}
			} else if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save") {
					/* Save item
					 */
					if ($this->model->item_ok($_POST) == false) {
						$this->show_alarm_form($_POST);
					} else if ($this->model->update_item($_POST) == false) {
						$this->view->add_message("Database error.");
						$this->show_alarm_form($_POST);
					} else {
						$this->show_list($_POST["list_id"]);
					}
				} else if ($_POST["submit_button"] == "Delete") {
					/* Delete item
					 */
					if ($this->model->delete_item($_POST) == false) {
						$this->view->add_message("Database error.");
						$this->show_alarm_form($_POST);
					} else {
						$this->show_list($_POST["list_id"]);
					}
				} else {
					$this->show_list($_POST["list_id"]);
				}
			} else if ($this->page->parameters[0] == "alarm") {
				/* Show edit item form
				 */
				if (($item = $this->model->get_item($this->page->parameters[1])) == false) {
					$this->view->add_tag("result", "Item not found", array("url" => "list"));
				} else {
					$this->show_alarm_form($item);
				}
			} else {
				/* Show list
				 */
				$this->show_list($this->page->parameters[0]);
			}
		}
	}
?>
