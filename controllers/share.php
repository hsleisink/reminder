<?php
	class share_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function show_forms($list_id) {
			$url = array("url" => "list");

			if (($list = $this->model->get_list($list_id)) == false) {
				$this->view->add_tag("result", "Invalid list id.", $url);
				return false;
			}

			$this->view->title = $list["name"];

			if (($friends = $this->model->get_friends($list_id)) === false) {
				$this->view->add_tag("result", "Database error.");
				return false;
			}

			if (($others = $this->model->get_others($list_id)) === false) {
				$this->view->add_tag("result", "Database error.");
				return false;
			}

			$this->view->add_javascript("share.js");

			$this->view->open_tag("share", array("list_id" => $list_id));

			/* Friends
			 */
			$this->view->open_tag("friends");
			foreach ($friends as $friend) {
				$friend["shared"] = show_boolean($friend["shared"]);
				$this->view->record($friend, "friend");
			}
			$this->view->close_tag();

			/* Others
			 */
			$this->view->open_tag("others");
			foreach ($others as $other) {
				$this->view->record($other, "other");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = "List";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->valid_list_id($_POST["list_id"]) == false) {
					$this->view->add_system_warning("Invalid list id.");
				} else if ($this->model->save_share($_POST["list_id"], $_POST) == false) {
					$this->view->add_system_warning("Database error.");
				} else {
					$this->view->add_system_message("Sharing saved.");
				}
				$this->show_forms($_POST["list_id"]);
			} else {
				$this->show_forms($this->page->parameters[0]);
			}
		}
	}
?>
