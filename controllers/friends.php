<?php
	class friends_controller extends Banshee\controller {
		private function show_overview() {
			if (($friends = $this->model->get_friends()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("overview", array("search" => $_SESSION["friends_search"] ?? ""));

			$this->view->open_tag("friends");
			foreach ($friends as $friend) {
				$this->view->record($friend, "friend");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Add") {
					/* Add friend
					 */
					if ($this->model->add_friend($_POST["address"]) === false) {
						$this->view->add_tag("friend", $_POST["address"]);
					} else {
						$this->user->log_action("user %s added to friends", $_POST["address"]);
					}
				} else if ($_POST["submit_button"] == "delete") {
					/* Delete friend
					 */
					if ($this->model->delete_friend($_POST["id"]) === false) {
						$this->view->add_message("Error deleting friend from friends list.");
					} else {
						$this->user->log_action("user %d deleted from friends", $_POST["id"]);
					}
				}
			}

			/* Show overview
			 */
			$this->show_overview();
		}
	}
?>
