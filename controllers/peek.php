<?php
	class peek_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function show_list() {
			$url = array("url" => "");

			if ($this->page->parameters[0] == "") {
				$this->view->add_tag("result", "No list specified.", $url);
				return;
			}

			if (($list = $this->model->get_list($this->page->parameters[0])) == false) {
				$this->view->add_tag("result", "List not found.", $url);
				return;
			}

			if (($items = $this->model->get_list_items($list["id"])) === false) {
				$this->view->add_tag("result", "Error fetching list items.", $url);
				return;
			}

			$_SESSION["access_code"] = $this->page->parameters[0];

			$this->view->add_javascript("peek.js");

			$this->view->title = $list["name"];

			$this->view->open_tag("list");
			foreach ($items as $item) {
				$item["marked"] = show_boolean($item["marked"]);
				if (is_true($item["marked"]) && in_array($item["id"], $_SESSION["items_marked"])) {
					$item["marked"] .= "-self";
				}
				$this->view->record($item, "item");
			}
			$this->view->close_tag();
		}

		private function mark_item($item_id) {
			if ($_SESSION["access_code"] == null) {
				return;
			}

			if (($list = $this->model->get_list($_SESSION["access_code"])) == false) {
				return false;
			}

			if ($this->model->item_marked($list["id"], $item_id) == false) {
				if ($this->model->set_mark($list["id"], $item_id, YES) == false) {
					return false;
				}

				array_push($_SESSION["items_marked"], $item_id);
				$mark = true;
			} else if (in_array($item_id, $_SESSION["items_marked"])) {
				if ($this->model->set_mark($list["id"], $item_id, NO) == false) {
					return false;
				}

				$mark = false;
			}

			$this->view->add_tag("result", show_boolean($mark));
		}

		public function execute() {
			$this->view->title = "Reminder list";

			if (is_array($_SESSION["items_marked"]) == false) {
				$_SESSION["items_marked"] = array();
			}

			if ($this->page->ajax_request) {
				if ($this->mark_item($_POST["item_id"]) == false) {
					$this->view->add_tag("result", "error");
				}
			} else {
				$this->show_list();
			}
		}
	}
?>
