var editing_item = null;
var busy_saving = false;

function key_pressed(e, input) {
	if (e.which == 13) {
		add_item(input);
	}
}

function button_pressed(button) {
	var input = $(button).parent().parent().find('input');
	add_item(input);
	$(input).focus();
}

function add_item(input) {
	var email = $(input).val().trim();

	if (email == '') {
		return;
	}

	$('ul.others').append('<li class="list-group-item">' + email + '<input type="hidden" name="others[]" value="' + email + '" /><span class="glyphicon glyphicon-remove remove" aria-hidden="true" onClick="javascript:remove_item(this)" /></li>');
	$(input).val('');
}

function remove_item(span) {
	$(span).parent().remove();
}

function strike(input) {
	$(input).parent().toggleClass('strike');
}
