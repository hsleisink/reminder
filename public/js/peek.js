$(document).ready(function() {
	$('ul.list li[marked!="no"]').addClass('marked');
	$('ul.list li[marked!="no"] input').prop('checked', true);
	$('ul.list li[marked="yes"] input').prop('disabled', true);
});

function toggle_mark(input, item_id) {
	var check = $(input).prop('checked');
	$(input).prop('checked', check == false);


	$.post('/peek', {
		item_id: item_id,
	}).done(function(data) {
		$(input).parent().toggleClass('marked');
		$(input).prop('checked', check);
    }).fail(function() {
    });
}
