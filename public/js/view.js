$(document).ready(function() {
	$('div.add input').focus();
});

var editing_item = null;
var busy_saving = false;

function key_pressed(e, input) {
	if (e.which == 13) {
		add_item(input);
	}
}

function button_pressed(button) {
	var input = $(button).parent().parent().find('input');
	add_item(input);
	$(input).focus();
}

function load_list(list_id) {
	var list = $('div.content ul.list');

	$.ajax('/view/' + list_id).done(function(data) {
		var code = $(data).find('result').attr('code');

		if (code != 200) {
			alert($(data).find('result').text());
			document.location = '/list';
		} else {
			$(data).find('result item').each(function(item) {
				var name = $(this).find('name').text();
				var link = $(this).find('link').text();
				var alarm = $(this).find('alarm').text() == 'yes';
				var marked = $(this).find('marked').text() == 'yes';
				add_text(list, $(this).attr('id'), name, link, alarm, marked);
			});
		}
		$('div#loading').remove();
	}).fail(function() {
		alert('Something went wrong.');
	});
}

function add_text(list, item_id, name, link, alarm, marked) {
	name = name.replace(/</g, '&lt;').replace(/>/g, '&gt;');

	var pos = name.indexOf(']');
	if ((name.substring(0, 1) == '[') && (pos > 0)) {
		name = '<span class="section">' + name.substr(1);
		name = name.replace(']', '</span>');
	}

	var type = $('ul.list').attr('list_type') == 0 ? 'delete' : 'strike';

	$(list).prepend('<li item_id="' + item_id + '" class="list-group-item"><div class="edit">' +
		(alarm ? '<span class="glyphicon glyphicon-bell" aria-hidden="true" />' : '') +
		'<a href="/view/alarm/' + item_id + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true" /></a>' +
		'</div><input type="checkbox" onClick="' + type + '_item(this)" />' + name + (link != '' ? ' [<a href="' + link + '" target="_blank">link</a>]' : '') + '</li>');

	if (marked) {
		$(list).find('li:first-child').addClass('marked');
		$(list).find('li:first-child input[type=checkbox]').prop('checked', true);
	}
}

function add_item(input) {
	var name = $(input).val().trim();
	var list = $(input).parent().parent().find('ul.list');
	var list_id = $(list).attr('list_id');

	if (name == '') {
		$(input).val('');
		return;
	}

	if (busy_saving) {
		return;
	}

	busy_saving = true;

	$.post('/view', {
		action: 'add',
		list_id: list_id,
		name: name
	}).done(function(data) {
		var result = $(data).find('result').text();
		var code = $(data).find('result').attr('code');

		if (code != 200) {
			if (editing_item != null) {
				$(editing_item).show();
				editing_item = null;
			}

			alert(result);
		} else {
			var item_id = Number.parseInt(result);
			add_text(list, item_id, name, '', false, false);

			$(input).val('');

			if (editing_item != null) {
				delete_item($(editing_item).find('input'));
				editing_item = null;
			}
		}

		busy_saving = false;
	}).fail(function() {
		if (editing_item != null) {
			$(editing_item).show();
			editing_item = null;
		}

		alert('Something went wrong.');

		busy_saving = false;
	});
}

function strike_item(checkbox) {
	var elem = $(checkbox).parent();
	var item_id = elem.attr('item_id');
	var mark = $(checkbox).prop('checked') ? 1 : 0;

	$.post('/view', {
		action: 'strike',
		id: item_id,
		mark: mark
	}).done(function(data) {
		var result = $(data).find('result').text();
		var code = $(data).find('result').attr('code');

		if (code != 200) {
			alert(result);
			$(checkbox).prop('checked', $(checkbox).prop('checked') == false);
		} else if (elem.hasClass('marked')) {
			elem.removeClass('marked');
		} else {
			elem.addClass('marked');
		}
	}).fail(function() {
		alert('something went wrong.');
	});
}

function delete_item(checkbox) {
	var elem = $(checkbox).parent();
	var item_id = elem.attr('item_id');

	$.post('/view', {
		action: 'delete',
		id: item_id
	}).done(function(data) {
		var result = $(data).find('result').text();
		var code = $(data).find('result').attr('code');

		if ((code != 200) && (code != 404)) {
			alert(result);
			$(checkbox).prop('checked', false);
		} else {
			elem.remove();
		}
	}).fail(function() {
		alert('something went wrong.');
	});
}

function refresh_list(list_id) {
	$('ul.list').empty();
	load_list(list_id);
}
