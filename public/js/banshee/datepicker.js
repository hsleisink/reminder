$(document).ready(function() {
	$("input.datepicker").datepicker({
		dateFormat: "DD, d MM yy",
		firstDay: 1,
		showButtonPanel: true,
		showWeek: true,
	});
});
