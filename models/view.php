<?php
	class view_model extends Banshee\model {
		public function valid_list_id($list_id) {
			$query = "select count(*) as count from lists where id=%d and owner_id=%d";
			if (($result = $this->db->execute($query, $list_id, $this->user->id)) == false) {
				return false;
			}

			if ($result[0]["count"] == 1) {
				return true;
			}

			$query = "select count(*) as count from list_shared where list_id=%d and user_id=%d";
			if (($result = $this->db->execute($query, $list_id, $this->user->id)) == false) {
				return false;
			}

			return $result[0]["count"] == 1;
		}

		public function get_list($list_id) {
			if ($this->valid_list_id($list_id) == false) {
				return false;
			}

			return $this->db->entry("lists", $list_id);
		}

		public function get_list_items($list_id) {
			$query = "select * from list_items where list_id=%d order by name desc";

			return $this->db->execute($query, $list_id);
		}

		public function get_item($item_id) {
			if (($item = $this->db->entry("list_items", $item_id)) == false) {
				return false;
			}

			if ($this->valid_list_id($item["list_id"]) == false) {
				return false;
			}

			return $item;
		}

		public function item_ok($item) {
			$result = true;

			if (trim($item["name"]) == "") {
				$this->view->add_message("Name can't be empty");
				$result = false;
			}

			return $result;
		}

		public function add_item($name, $list_id) {
			$item = array(
				"id"      => null,
				"list_id" => $list_id,
				"name"    => substr($name, 0, $this->settings->list_item_max_length),
				"link"    => null,
				"marked"  => NO);
			if ($this->db->insert("list_items", $item) === false) {
				return false;
			}

			return $this->db->last_insert_id;
		}

		public function update_item($item) {
			$keys = array("name", "link", "alarm", "repeat", "marked");

			if (trim($item["link"]) == "") {
				$item["link"] = null;
			}

			if (trim($item["alarm"]) == "") {
				$item["alarm"] = null;
				$item["repeat"] = null;
			} else {
				$item["alarm"] = date("Y-m-d", strtotime($item["alarm"]));
			}

			$item["marked"] = is_true($item["marked"]) ? YES : NO;

			return $this->db->update("list_items", $item["id"], $item, $keys) !== false;
		}


		public function get_list_for_item($item_id) {
			$query = "select list_id from list_items where id=%d";
			if (($result = $this->db->execute($query, $item_id)) == false) {
				return false;
			}

			return $result[0]["list_id"];
		}

		public function mark_item($item_id, $mark) {
			$query = "update list_items set marked=%d where id=%d";

			return $this->db->query($query, $mark, $item_id) !== false;
		}

		public function delete_item($item_id) {
			return $this->db->delete("list_items", $item_id) !== false;
		}
	}
?>
