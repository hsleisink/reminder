<?php
	class share_model extends Banshee\model {
		public function valid_list_id($list_id) {
			return $this->borrow("list")->valid_list_id($list_id);
		}

		public function get_list($list_id) {
			if ($this->valid_list_id($list_id) == false) {
				return false;
			}

			return $this->db->entry("lists", $list_id);
		}

		public function get_friends($list_id) {
			$query = "select id, fullname from users where (organisation_id=%d and id!=%d) or ".
			         "id in (select friend_id from friends where user_id=%d) order by fullname";

			if (($friends = $this->db->execute($query, $this->user->organisation_id, $this->user->id, $this->user->id)) === false) {
				return false;
			}

			$query = "select user_id from list_shared where list_id=%d";
			if (($shared = $this->db->execute($query, $list_id)) === false) {
				return false;
			}

			foreach ($friends as $f => $friend) {
				$friends[$f]["shared"] = false;
				foreach ($shared as $s => $share) {
					if ($friend["id"] == $share["user_id"]) {
						$friends[$f]["shared"] = true;
						break;
					}
				}
			}

			return $friends;
		}

		public function save_share($list_id, $share) {
			$this->db->query("begin");

			/* Flush friends
			 */
			$query = "delete from list_shared where list_id=%d";
			if ($this->db->execute($query, $list_id) === false) {
				$this->db->query("rollback");
				return false;
			}

			/* Add friends
			 */
			if (is_array($share["friends"])) {
				foreach ($share["friends"] as $user_id) {
					if ($user_id == $this->user->id) {
						continue;
					}

					$data = array(
						"list_id" => $list_id,
						"user_id" => $user_id);
					if ($this->db->insert("list_shared", $data) === false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			/* Remove others
			 */
			if (is_array($share["remove"])) {
				$query = "delete from list_others where id=%d and list_id=%d";
				foreach ($share["remove"] as $id) {
					if ($this->db->query($query, $id, $list_id) === false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			/* Add others
			 */
			$access_codes = array();
			if (is_array($share["others"])) {
				foreach ($share["others"] as $other) {
					if (valid_email($other) == false) {
						$this->view->add_message($other. " is not a valid e-mail address.");
						continue;
					}

					$access_code = random_string(20);
					$data = array(
						"list_id"     => $list_id,
						"email"       => $other,
						"access_code" => $access_code);
					if ($this->db->insert("list_others", $data) === false) {
						$this->db->query("rollback");
						return false;
					}

					$access_codes[$other] = $access_code;
				}
			}

			if ($this->db->query("commit") === false) {
				return false;
			}

			/* Send mails
			 */
			if (is_array($share["others"])) {
				if (($list = $this->get_list($list_id)) == false) {
					$list = array(
						"name" => "List");
				}

				$fields = array(
					"LIST"     => $list["name"],
					"PROTOCOL" => $_SERVER["HTTP_SCHEME"],
					"HOSTNAME" => $_SERVER["SERVER_NAME"],
					"TITLE"    => $this->settings->head_title,
					"FULLNAME" => $this->user->fullname);

				foreach ($share["others"] as $other) {
					if (valid_email($other) == false) {
						continue;
					}

					$fields["ACCESS_CODE"] = $access_codes[$other];

					$email = new \Banshee\Protocols\email($list["name"]." list shared", $other);
					$email->message(file_get_contents("../extra/list_shared.txt"));
					$email->set_message_fields($fields);
					$email->send($other);
					unset($email);
				}
			}

			return true;
		}

		public function get_others($list_id) {
			$query = "select * from list_others where list_id=%d";

			return $this->db->execute($query, $list_id);
		}
	}
?>
