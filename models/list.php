<?php
	class list_model extends Banshee\model {
		public function get_lists() {
			$query = "select *, (select count(*) from list_items where list_id=l.id) as items ".
			         "from lists l where owner_id=%d or id in ".
			         "(select list_id from list_shared where user_id=%d) order by name";

			return $this->db->execute($query, $this->user->id, $this->user->id);
		}

		public function valid_list_id($list_id) {
			$query = "select count(*) as count from lists where id=%d and owner_id=%d";
			if (($result = $this->db->execute($query, $list_id, $this->user->id)) == false) {
				return false;
			}

			return $result[0]["count"] == 1;
		}

		public function get_list($list_id) {
			if ($this->valid_list_id($list_id) == false) {
				return false;
			}

			return $this->db->entry("lists", $list_id);
		}

		public function save_oke($list) {
			$result = true;

			if (trim($list["name"]) == "") {
				$this->view->add_message("Invalid list name");
				$result = false;
			}

			if (isset($list["id"])) {
				if ($this->valid_list_id($list["id"]) == false) {
					$this->view->add_message("Unknown list.");
					$result = false;
				}
			}

			return $result;
		}

		public function create_list($list) {
			$keys = array("id", "name", "owner_id", "type");

			$list["id"] = null;
			$list["owner_id"] = $this->user->id;

			return $this->db->insert("lists", $list, $keys);
		}

		public function update_list($list) {
			$keys = array("name", "type");

			return $this->db->update("lists", $list["id"], $list, $keys);
		}

		public function unstrike_list($list_id) {
			$query = "update list_items set marked=%d where list_id=%d";

			return $this->db->query($query, NO, $list_id) !== false;
		}

		public function leave_list($list_id) {
			$query = "delete from list_shared where list_id=%d and user_id=%d";

			return $this->db->query($query, $list_id, $this->user->id) !== false;
		}

		public function delete_oke($list) {
			$result = true;

			if ($this->valid_list_id($list["id"]) == false) {
				$this->view->add_message("Unknown list");
				$result = false;
			}

			return $result;
		}

		public function delete_list($list_id) {
			$queries = array(
				array("delete from list_others where list_id=%d", $list_id),
				array("delete from list_shared where list_id=%d", $list_id),
				array("delete from list_items where list_id=%d", $list_id),
				array("delete from lists where id=%d", $list_id));

			return $this->db->transaction($queries);
		}
	}
?>
