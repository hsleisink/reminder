<?php
	class friends_model extends Banshee\model {
		private $columns = array();

		public function get_friends() {
			if ($this->user->organisation_id != DEFAULT_ORGANISATION_ID) {
				$query = "select id, fullname, email from users where organisation_id=%d and id!=%d and status!=%d";
				if (($users = $this->db->execute($query, $this->user->organisation_id, $this->user->id, USER_STATUS_DISABLED)) === false) {
					return false;
				}
			} else {
				$users = array();
			}

			$query = "select u.id, a.id as friend_id, u.fullname, u.email from users u, friends a ".
			         "where u.id=a.friend_id and a.user_id=%d";
			if (($friends = $this->db->execute($query, $this->user->id)) === false) {
				return false;
			}

			return array_merge($users, $friends);
		}

		public function add_friend($friend) {
			if (($friend = $this->db->entry("users", $friend, "email")) == false) {
				$this->view->add_message("E-mail address not found.");
				return false;
			}

			/* Check in organisation
			 */
			if ($friend["organisation_id"] == $this->user->organisation_id) {
				$this->view->add_message("Friend is already on your list.");
				return false;
			}

			/* Check already in addresbook
			 */
			$query = "select count(*) as count from friends where user_id=%d and friend_id=%d";
			if (($result = $this->db->execute($query, $this->user->id, $friend["id"])) == false) {
				$this->view->add_message("Database error.");
				return false;
			}

			if ($result[0]["count"] > 0) {
				$this->view->add_message("Friend is already on your list.");
				return false;
			}

			$keys = array("id", "user_id", "friend_id");

			$friends["id"] = null;
			$friends["user_id"] = $this->user->id;
			$friends["friend_id"] = (int)$friend["id"];

			if ($this->db->insert("friends", $friends, $keys) == false) {
				$this->view->add_message("Database error.");
				return false;
			}

			return true;
		}

		public function delete_friend($friend_id) {
			$query = "delete from friends where friend_id=%d and user_id=%d";
			return $this->db->query($query, $friend_id, $this->user->id) !== false;
		}
	}
?>
