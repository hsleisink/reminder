<?php
	class peek_model extends Banshee\model {
		public function get_list($access_code) {
			$query = "select l.id, l.name from lists l, list_others o where l.id=o.list_id and o.access_code=%s";

			if (($result = $this->db->execute($query, $access_code)) == false) {
				return false;
			}

			return $result[0];
		}

		public function get_list_items($list_id) {
			$query = "select id, name, marked from list_items where list_id=%d order by name";

			return $this->db->execute($query, $list_id);
		}

		public function item_marked($list_id, $item_id) {
			$query = "select marked from list_items where id=%d and list_id=%d";

			if (($result = $this->db->execute($query, $item_id, $list_id)) === false) {
				return false;
			}

			return is_true($result[0]["marked"]);
		}

		public function set_mark($list_id, $item_id, $mark) {
			$query = "update list_items set marked=%d where id=%d and list_id=%d";

			return $this->db->query($query, $mark, $item_id, $list_id) !== false;
		}
	}
?>
