<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Share template
//
//-->
<xsl:template match="share">
<xsl:call-template name="show_messages" />

<form id="share" method="post" action="/{/output/page}/{@list_id}">
<input type="hidden" name="list_id" value="{@list_id}" />

<h2>Reminder List users</h2>
<ul class="list-group friends">
<xsl:for-each select="friends/friend">
<li class="list-group-item"><input type="checkbox" name="friends[]" value="{@id}"><xsl:if test="shared='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><xsl:value-of select="fullname" /></li>
</xsl:for-each>
</ul>

<h2>Other people</h2>
<ul class="list-group others">
<xsl:for-each select="others/other">
<li class="list-group-item"><xsl:value-of select="email" /><input type="checkbox" name="remove[]" value="{@id}" class="remove" onClick="javascript:strike(this)" /></li>
</xsl:for-each>
</ul>
</form>

<div class="input-group add">
<input type="text" id="new_other" class="form-control" placeholder="New e-mail address..." onKeyPress="javascript:key_pressed(event, this)" />
<span class="input-group-btn">
<button class="btn btn-default" type="button" onClick="javascript:button_pressed(this)">Add</button>
</span>
</div>


<div class="btn-group">
<input type="button" name="submit_button" value="Save sharing" class="btn btn-default" onClick="javascript:$('form#share').submit()" />
<a href="/list" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/users.png" class="title_icon" />
<h1>Share <xsl:value-of select="../layout/title/@page" /></h1>
<xsl:apply-templates select="share" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
