
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  List  template
//
//-->
<xsl:template match="list">
<div class="input-group add">
<input type="text" class="form-control" maxlength="{@maxlength}" autofocus="autofocus" placeholder="New item name..." onKeyPress="javascript:key_pressed(event, this)" />
<span class="input-group-btn">
<button class="btn btn-default" type="button" onClick="javascript:button_pressed(this)">+</button>
<button class="btn btn-default" onClick="javascript:refresh_list({@id})">&#8634;</button>
<a href="/list" class="btn btn-default">&#8626;</a>
</span>
</div>

<div id="loading">Loading list...</div>
<ul list_id="{@id}" list_type="{@type}" class="list-group list"></ul>
</xsl:template>

<!--
//
//  Edit  template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />

<form action="/{/output/page}/{item/list_id}" method="post">
<input type="hidden" name="id" value="{item/@id}" />
<input type="hidden" name="list_id" value="{item/list_id}" />
<label for="name">Name:</label>
<input type="text" id="name" name="name" value="{item/name}" class="form-control" />
<label for="name">Link:</label>
<input type="text" id="link" name="link" value="{item/link}" class="form-control" />
<label for="alarm">Alarm:</label>
<div class="input-group">
<input type="text" id="alarm" name="alarm" value="{item/alarm}" class="form-control datepicker" />
<span class="input-group-btn">
<button class="btn btn-default" type="button" onClick="javascript:$('input#alarm').val(''); $('select#repeat').val(0)"><span class="glyphicon glyphicon-remove" aria-hidden="true" /></button>
</span>
</div>
<label for="repeat">Repeat alarm:</label>
<select id="repeat" name="repeat" class="form-control">
<xsl:for-each select="alarm_repeat/option"><option value="{@value}"><xsl:if test="@value=../../item/repeat"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="marked">Marked:</label>
<input type="checkbox" name="marked"><xsl:if test="item/marked='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save" class="btn btn-default" />
<input type="submit" name="submit_button" value="Delete" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
<a href="/{/output/page}/{item/list_id}" class="btn btn-default">Back</a>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/list.png" class="title_icon" />
<h1><xsl:value-of select="../layout/title/@page" /></h1>
<xsl:apply-templates select="list" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
