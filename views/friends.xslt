<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />

<table class="table table-condensed table-striped">
<thead>
<tr><th>Name</th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="friends/friend">
<xsl:sort select="fullname" />
<tr>
<td><xsl:value-of select="fullname" /></td>
<td><xsl:if test="friend_id"><form action="/{/output/page}" method="post"><input type="hidden" name="submit_button" value="delete" /><input type="hidden" name="id" value="{@id}" /><a href="#" onClick="javascript:if (confirm('DELETE: Are you sure?')) $(this).parent().submit()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></form></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<form method="post" action="/{/output/page}">
<div class="input-group">
<input type="input" name="address" value="{../address}" placeholder="E-mail address" class="form-control" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Add" class="btn btn-default" /></span>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/friends.png" class="title_icon" />
<h1>Friends</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
