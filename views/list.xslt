<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />
<xsl:import href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-striped table-hover">
<thead>
<tr>
<th>Name</th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="lists/list">
<tr>
<td class="click" onClick="javascript:document.location='/view/{@id}'"><xsl:value-of select="name" /><span class="badge"><xsl:value-of select="items" /></span></td>
<td><xsl:if test="owner_id=/output/user/@id"><a href="/{/output/page}/{@id}"><span class="glyphicon glyphicon-pencil" aria-hidden="true" /></a></xsl:if></td>
<td><xsl:choose><xsl:when test="owner_id=/output/user/@id"><a href="/share/{@id}"><span class="glyphicon glyphicon-user" aria-hidden="true" /></a></xsl:when><xsl:otherwise><form action="/{/output/page}" method="post"><input type="hidden" name="submit_button" value="Leave list" /><input type="hidden" name="id" value="{@id}" /><a href="#" onClick="javascript:if (confirm('LEAVE: Are you sure?')) $(this).parent().submit()"><span class="glyphicon glyphicon-remove" aria-hidden="true" /></a></form></xsl:otherwise></xsl:choose></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/{/output/page}/new" class="btn btn-default">New list</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="list/@id">
<input type="hidden" name="id" value="{list/@id}" />
</xsl:if>

<label for="name">Name:</label>
<input type="text" id="name" name="name" value="{list/name}" autofocus="autofocus" class="form-control" />
<label for="type">Type:</label>
<select id="type" name="type" class="form-control">
<xsl:for-each select="types/type">
<option value="{@value}"><xsl:if test="@value=../../list/type"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<xsl:if test="list/@id">
<label for="type">Unstrike all items:</label>
<p><input type="checkbox" name="unstrike" /></p>
</xsl:if>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save list" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="list/@id">
<input type="submit" name="submit_button" value="Delete list" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/list.png" class="title_icon" />
<h1>Lists</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
