<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  List template
//
//-->
<xsl:template match="list">
<ul class="list-group list">
<xsl:for-each select="item">
<li class="list-group-item" marked="{marked}"><input type="checkbox" onClick="javascript:toggle_mark(this, {@id})" /><xsl:value-of select="name" /></li>
</xsl:for-each>
</ul>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1><xsl:value-of select="/output/layout/title/@page" /></h1>
<xsl:apply-templates select="list" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
