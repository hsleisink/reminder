<?php
	define("REMINDER_VERSION", "0.2");

	define("LIST_TYPE_DELETE",     0);
	define("LIST_TYPE_STRIKE",     1);

	define("ALARM_REPEAT_NONE",    0);
	define("ALARM_REPEAT_DAILY",   1);
	define("ALARM_REPEAT_WEEKLY",  2);
	define("ALARM_REPEAT_MONTHLY", 3);
	define("ALARM_REPEAT_YEARLY",  4);
?>
